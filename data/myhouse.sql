/*
Navicat MySQL Data Transfer

Source Server         : aaa
Source Server Version : 50529
Source Host           : 127.0.0.1:3306
Source Database       : ssm_px

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2020-10-16 09:15:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for myarea
-- ----------------------------
DROP TABLE IF EXISTS `myarea`;
CREATE TABLE `myarea` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `aname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myarea
-- ----------------------------
INSERT INTO `myarea` VALUES ('1', '岳麓区');
INSERT INTO `myarea` VALUES ('2', '望城区');
INSERT INTO `myarea` VALUES ('3', '芙蓉区');
INSERT INTO `myarea` VALUES ('4', '高新区');
INSERT INTO `myarea` VALUES ('5', '星沙县');
INSERT INTO `myarea` VALUES ('6', '长沙县');

-- ----------------------------
-- Table structure for mybiao
-- ----------------------------
DROP TABLE IF EXISTS `mybiao`;
CREATE TABLE `mybiao` (
  `bid` bigint(20) NOT NULL AUTO_INCREMENT,
  `hid` bigint(20) DEFAULT NULL,
  `dkd` float DEFAULT NULL,
  `skd` float DEFAULT NULL,
  `mkd` float DEFAULT NULL,
  `mtime` varchar(20) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mybiao
-- ----------------------------
INSERT INTO `mybiao` VALUES ('1', '2', '12', '88', '66', '1602594265123', '1');
INSERT INTO `mybiao` VALUES ('2', '2', '99', '88', '11', '1602593733348', '1');
INSERT INTO `mybiao` VALUES ('3', '3', '0', '0', '0', '1602724292402', '1');
INSERT INTO `mybiao` VALUES ('4', '1', '100', '88', '66', '1602724251297', '1');

-- ----------------------------
-- Table structure for mybs
-- ----------------------------
DROP TABLE IF EXISTS `mybs`;
CREATE TABLE `mybs` (
  `bid` bigint(20) NOT NULL AUTO_INCREMENT,
  `hid` bigint(20) DEFAULT NULL,
  `mtime` varchar(20) DEFAULT NULL,
  `bremark` varchar(500) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mybs
-- ----------------------------
INSERT INTO `mybs` VALUES ('1', '2', '1602729517143', '房屋漏水', '1');
INSERT INTO `mybs` VALUES ('2', '1', '1602731437362', '门锁损坏', '1');

-- ----------------------------
-- Table structure for mycus
-- ----------------------------
DROP TABLE IF EXISTS `mycus`;
CREATE TABLE `mycus` (
  `cid` bigint(20) NOT NULL AUTO_INCREMENT,
  `cname` varchar(20) DEFAULT NULL,
  `csex` varchar(2) DEFAULT NULL,
  `ctel` varchar(20) DEFAULT NULL,
  `ctel1` varchar(20) DEFAULT NULL,
  `ccard` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mycus
-- ----------------------------
INSERT INTO `mycus` VALUES ('1', '张三', '男', '13259993383', '13259993383', '123121232131224211');
INSERT INTO `mycus` VALUES ('2', '李四', '男', '18740429441', '18740429441', '18740429441333333');
INSERT INTO `mycus` VALUES ('3', '王五', '男', '18740429441', '18740429441', '612501200003033197');
INSERT INTO `mycus` VALUES ('4', '赵六', '男', '18740429441', '18740429441', '61250120000303319X');
INSERT INTO `mycus` VALUES ('5', 'ss', '女', '18740429441', '18740429441', '18740429441333333');

-- ----------------------------
-- Table structure for mydept
-- ----------------------------
DROP TABLE IF EXISTS `mydept`;
CREATE TABLE `mydept` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(50) DEFAULT NULL,
  `pflag` int(11) DEFAULT NULL,
  `premark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mydept
-- ----------------------------
INSERT INTO `mydept` VALUES ('1', '管理规划部', '0', '管理部门,不可删除');
INSERT INTO `mydept` VALUES ('2', '房管一部', '0', '主要负责房源信息录入');
INSERT INTO `mydept` VALUES ('3', '房管二部', '0', '主要负责房子出租');
INSERT INTO `mydept` VALUES ('4', '房管三部', '0', '主要负责房子出租');
INSERT INTO `mydept` VALUES ('5', 'xxx', '0', 'sss');

-- ----------------------------
-- Table structure for mydj
-- ----------------------------
DROP TABLE IF EXISTS `mydj`;
CREATE TABLE `mydj` (
  `mid` bigint(20) NOT NULL AUTO_INCREMENT,
  `mdate` bigint(20) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  `cid` bigint(20) DEFAULT NULL,
  `hid` bigint(20) DEFAULT NULL,
  `mimg` varchar(200) DEFAULT NULL,
  `myj` float DEFAULT NULL,
  `myzj` float DEFAULT NULL,
  `mflag` int(11) DEFAULT '0',
  `mbegintime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mydj
-- ----------------------------
INSERT INTO `mydj` VALUES ('1', '1602686863100', '1', '1', '1', '/upload/2020-10-14/timg.jpg', '23232', '23232', '0', '1602604800000');
INSERT INTO `mydj` VALUES ('2', '1602686883089', '1', '2', '1', '/upload/2020-10-14/timg.jpg', '233', '2333', '0', '1602691200000');
INSERT INTO `mydj` VALUES ('3', '1602722004871', '1', '1', '3', '/upload/2020-10-15/timg (1).jpg', '2000', '3000', '1', '1603900800000');
INSERT INTO `mydj` VALUES ('4', '1602723956877', '1', '3', '2', '/upload/2020-10-15/timg (1).jpg', '2000', '6000', '1', '1603814400000');

-- ----------------------------
-- Table structure for myemp
-- ----------------------------
DROP TABLE IF EXISTS `myemp`;
CREATE TABLE `myemp` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `jid` int(11) DEFAULT NULL,
  `ename` varchar(20) DEFAULT NULL,
  `epsw` varchar(20) DEFAULT NULL,
  `erealname` varchar(20) DEFAULT NULL,
  `etel` varchar(20) DEFAULT NULL,
  `eaddress` varchar(200) DEFAULT NULL,
  `eflag` int(11) DEFAULT NULL,
  `eremark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myemp
-- ----------------------------
INSERT INTO `myemp` VALUES ('1', '0', '0', 'admin', 'admin', '赵本山', '13995668339', '长沙市岳麓区120号', '0', '此人为系统管理员，不可删除');
INSERT INTO `myemp` VALUES ('2', '2', '2', 'liuxq', '123', '刘小庆', '13995668330', '长沙市望城区121号', '0', '优秀的置业顾问');
INSERT INTO `myemp` VALUES ('3', '3', '2', 'zhaozw', '123', '赵子武', '15895668330', '长沙市岳麓区122号', '0', '优秀的置业顾问');
INSERT INTO `myemp` VALUES ('4', '3', '2', 'zhanghy', '123', '张海洋', '13095668331', '长沙市望城区123号', '0', '优秀的置业顾问');

-- ----------------------------
-- Table structure for myhouse
-- ----------------------------
DROP TABLE IF EXISTS `myhouse`;
CREATE TABLE `myhouse` (
  `hid` bigint(20) NOT NULL AUTO_INCREMENT,
  `sid` int(11) DEFAULT NULL,
  `aid` int(11) DEFAULT NULL,
  `haddress` varchar(200) DEFAULT NULL,
  `hfh` varchar(50) DEFAULT NULL,
  `hhx` varchar(50) DEFAULT NULL,
  `hmj` varchar(20) DEFAULT NULL,
  `hcx` varchar(20) DEFAULT NULL,
  `hmoney` float DEFAULT NULL,
  `hwf` float DEFAULT NULL,
  `hdx` float DEFAULT NULL,
  `hsf` float DEFAULT NULL,
  `hmq` float DEFAULT NULL,
  `dkd` float DEFAULT NULL,
  `skd` float DEFAULT NULL,
  `mkd` float DEFAULT NULL,
  `hjp` varchar(20) DEFAULT NULL,
  `hremark` varchar(500) DEFAULT NULL,
  `himg` varchar(500) DEFAULT NULL,
  `hflag` int(11) DEFAULT NULL,
  PRIMARY KEY (`hid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myhouse
-- ----------------------------
INSERT INTO `myhouse` VALUES ('1', '2', '2', '岳麓区保利花园小区', 'A区4栋2单元801室', '3室2厅2卫', '130', '坐北朝南', '3500', '1800', '0.5', '1.2', '1.2', '456', '456', '789', 'BLHY', '备注说明', 'upload/1.jpg、upload/2.jpg、upload/3.jpg', '0');
INSERT INTO `myhouse` VALUES ('2', '1', '1', '东城花园', 'B区1栋5单元303室', '3室1厅1卫', '250', '坐南朝北', '2000', '1500', '0.5', '1', '1', '400', '400', '600', 'NCHY', '备注说明', 'upload/1.jpg、upload/2.jpg、upload/3.jpg', '0');
INSERT INTO `myhouse` VALUES ('3', '4', '3', '海富城停车场', '1号停车位', '单个车位', '15', '东西车位', '500', '0', '0', '0', '0', '0', '0', '0', 'HFCP', '标准车位', '/upload/2020-09-29/54a557e85e1b42ff8e2a1b838f0b5c51.jpg、/upload/2020-09-29/f98218914ecc4e75bd0fc82fe532ed60.jpg', '0');

-- ----------------------------
-- Table structure for myht
-- ----------------------------
DROP TABLE IF EXISTS `myht`;
CREATE TABLE `myht` (
  `htid` int(11) NOT NULL AUTO_INCREMENT,
  `htname` varchar(50) DEFAULT NULL,
  `htremark` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`htid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myht
-- ----------------------------
INSERT INTO `myht` VALUES ('1', '租房合同', 'aaaaaaaaaz。');
INSERT INTO `myht` VALUES ('2', 'bbbb', null);

-- ----------------------------
-- Table structure for myjs
-- ----------------------------
DROP TABLE IF EXISTS `myjs`;
CREATE TABLE `myjs` (
  `jid` int(11) NOT NULL AUTO_INCREMENT,
  `jname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`jid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myjs
-- ----------------------------
INSERT INTO `myjs` VALUES ('1', '系统管理员');
INSERT INTO `myjs` VALUES ('2', '置业顾问');
INSERT INTO `myjs` VALUES ('3', '会计');
INSERT INTO `myjs` VALUES ('4', 'boss');

-- ----------------------------
-- Table structure for mysf
-- ----------------------------
DROP TABLE IF EXISTS `mysf`;
CREATE TABLE `mysf` (
  `yid` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` bigint(20) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  `myzj` double DEFAULT NULL,
  `mbegintime` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`yid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mysf
-- ----------------------------
INSERT INTO `mysf` VALUES ('1', '1', '1', '2000', '1600876800000');
INSERT INTO `mysf` VALUES ('2', '4', '1', '8000', '1600876800000');
INSERT INTO `mysf` VALUES ('3', '2', '1', '23232', '1601913600000');
INSERT INTO `mysf` VALUES ('10', '1', '1', '6000', '1603900800000');
INSERT INTO `mysf` VALUES ('11', '4', '1', '3000', '1604678400000');

-- ----------------------------
-- Table structure for myshouru
-- ----------------------------
DROP TABLE IF EXISTS `myshouru`;
CREATE TABLE `myshouru` (
  `sid` bigint(20) NOT NULL AUTO_INCREMENT,
  `eid` int(11) DEFAULT NULL,
  `smoney` float DEFAULT NULL,
  `stm` varchar(20) DEFAULT NULL,
  `stime` varchar(20) DEFAULT NULL,
  `sremark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myshouru
-- ----------------------------
INSERT INTO `myshouru` VALUES ('1', '1', '5000', '客户物损赔偿', '1602598778404', '赔偿金');
INSERT INTO `myshouru` VALUES ('2', '1', '30000', '客户房租', '1602597469641', '收房租');

-- ----------------------------
-- Table structure for mysort
-- ----------------------------
DROP TABLE IF EXISTS `mysort`;
CREATE TABLE `mysort` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mysort
-- ----------------------------
INSERT INTO `mysort` VALUES ('1', '标准住宅');
INSERT INTO `mysort` VALUES ('2', '标准厂房');
INSERT INTO `mysort` VALUES ('3', '标准仓库');
INSERT INTO `mysort` VALUES ('4', '标准车位');

-- ----------------------------
-- Table structure for mytf
-- ----------------------------
DROP TABLE IF EXISTS `mytf`;
CREATE TABLE `mytf` (
  `tid` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` bigint(20) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  `tyzj` float DEFAULT NULL,
  `mtime` varchar(20) DEFAULT NULL,
  `mremark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mytf
-- ----------------------------
INSERT INTO `mytf` VALUES ('1', '1', '1', '1000', '1600876800000', 'aaaa');
INSERT INTO `mytf` VALUES ('2', '1', '1', '1000', '1601913600000', 'aaaaaaaaaax');
INSERT INTO `mytf` VALUES ('3', '1', '1', '1000', '1602691200000', '其他');

-- ----------------------------
-- Table structure for myzc
-- ----------------------------
DROP TABLE IF EXISTS `myzc`;
CREATE TABLE `myzc` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `ctitle` varchar(200) DEFAULT NULL,
  `ctime` varchar(20) DEFAULT NULL,
  `cremark` text,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myzc
-- ----------------------------
INSERT INTO `myzc` VALUES ('1', '鼓励住房租赁消费', '1602685719305', '非本地户籍承租人可按照《居住证暂行条例》等有关规定申领居住证，享受义务教育、医疗等国家规定的基本公共服务。');
INSERT INTO `myzc` VALUES ('2', '完善公共租赁住房', '1602685742902', '推进公租房货币化。转变公租房保障方式，实物保障与租赁补贴并举。');
INSERT INTO `myzc` VALUES ('3', '培育市场供应主体', '1602685794531', '<span style=\"background-color:;\">规范住房租赁中介机构。充分发挥中介机构作用，提供规范的居间服务。努力提高中介服务质量，不断提升从业人员素质，促进中介机构依法经营、诚实守信、公平交易。</span>');

-- ----------------------------
-- Table structure for myzhichu
-- ----------------------------
DROP TABLE IF EXISTS `myzhichu`;
CREATE TABLE `myzhichu` (
  `zid` bigint(20) NOT NULL AUTO_INCREMENT,
  `eid` int(11) DEFAULT NULL,
  `zmoney` float DEFAULT NULL,
  `ztm` varchar(20) DEFAULT NULL,
  `ztime` varchar(20) DEFAULT NULL,
  `zremark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`zid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of myzhichu
-- ----------------------------
INSERT INTO `myzhichu` VALUES ('1', '1', '500', '常用物件', '1602601954465', '物件添置');
INSERT INTO `myzhichu` VALUES ('2', '1', '440', '房屋维修', '1602601985426', '维修费');
