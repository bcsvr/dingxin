<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>无标题文档</title>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/style.css"></link>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/font-awesome.min.css"></link>
		<script src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
		<script src="${pageContext.request.contextPath}/layer/layer.js"></script>
		<script src="${pageContext.request.contextPath}/js/zhichu/myzhichu.js"></script>
		<SCRIPT type="text/javascript">
		//////////////权限控制！！！//////////////
		//var $userLevel=parseInt("${loginUser.EAdmin}");//获取登陆者的权限等级
		var $webName="${pageContext.request.contextPath}";
		//$(function(){
		//	var tools=$(".tools");
		//	if($userLevel!=-1){
		//		tools.hide();
		//	}
		//});
</SCRIPT>
	</head>
	<body>
	
	   <img alt="" src="" id="x" style="display: none;">
	
		<!-- 位置信息 -->
		<div class="place">
			<span>位置：</span>
			<ul class="placeul">
				<li>首页</li>
				<li>现金支出管理</li>
				<li>现金支出内容</li>
			</ul>
		</div>
		<!-- 内容表格信息 -->
		<div class="rightinfo">
			<div class="tools">
				<ul class="toolbar">
					<li class="addPart">
						<span><img src="${pageContext.request.contextPath }/images/t01.png" /> </span>添加
					</li>
				</ul>
				<ul class="toolbar">
					<li class="seAll">
						<span><img src="${pageContext.request.contextPath }/images/t01.png" /> </span>支出分析图
					</li>
				</ul>
				
			</div>
			<table class="tablelist"></table>
			<%-- 分页位置 --%>
			<div class="pagin">
				<div class="message"></div>
			</div>
		</div>
		<div id="tubiao" style="padding-left: 10px;"></div>
	</body>
</html>
