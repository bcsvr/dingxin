<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

<link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.js"></script>

<script language="javascript">
	$(function(){
    $('.error').css({'position':'absolute','left':($(window).width()-490)/2});
	$(window).resize(function(){  
    $('.error').css({'position':'absolute','left':($(window).width()-490)/2});
    })  
});  
</script> 
	</head>

	<body style="background: #FFF8ED;">

		<div class="tech">

			<dl>
				<dt>
					技术支持
				</dt>
				<dd>
					<b>湖南万树IT科技有限公司：</b>&nbsp;&nbsp;&nbsp;&nbsp; QQ：1847593810
				</dd>
				<dd>
					<b>电子邮件：</b>&nbsp;&nbsp;&nbsp;&nbsp;1847593810@qq.com
				</dd>
				<dd>
					<b>交流QQ群：</b>&nbsp;&nbsp;&nbsp;&nbsp;1108030092
				</dd>
			</dl>
		</div>
	</body>

</html>
