<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link href="${pageContext.request.contextPath }/css/style.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/sapar.css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/system_index.css" />
<script src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.messager.js"></script>
<script src="${pageContext.request.contextPath}/layer/layer.js"></script>
<script src="${pageContext.request.contextPath}/js/right/myright.js"></script>
<SCRIPT type="text/javascript">
	var $webName = "${pageContext.request.contextPath}";
</SCRIPT>
<script type="text/javascript">
	$(function() {
		$.messager.lays(250, 150);
		var num = 0;
		var mytx = "<div id='tx'>";
		$.ajax({
			url : '${pageContext.request.contextPath}/dj_allCount.action',
			data : '',
			type : 'post',
			dataType : 'json',
			success : function(data) {
				var count = data;
				num = data;
				mytx = "<a href='${pageContext.request.contextPath}/djsf/all.jsp'><font color=red>你有"
								+ count + "条待收租消息</font></a>";
				$("#tx").append(mytx);
				if (num != 0) {
					$.messager.show('您有新消息', mytx, 0);
				}
			}
		});

	});
</script>
</head>

<body>
	<div id="saper-container">
		<div id="saper-bd">
			<div>
				<div class="main-wrap">
					<!-- 左边部分 -->
					<div class="subfiled-wrap">

						<div class="place">
							<span>位置：</span>
							<ul class="placeul">
								<li>首页</li>

							</ul>
						</div>

						<div class="subfiled-content">
							<div class="sapar-form">
								<div class="kv-item clearfix">
									<div class="kv-item-content">


										<img src="${pageContext.request.contextPath }/images/zzz.gif"
											width="596" height="401" usemap="#Map" border="0" />
										<map name="Map" id="Map">
											<area shape="rect" coords="115,31,223,86"
												href="${pageContext.request.contextPath }/redirect.do?page=custom/all" />
											<area shape="rect" coords="246,19,353,73"
												href="${pageContext.request.contextPath }/redirect.do?page=djrz/all" />
											<area shape="rect" coords="364,42,464,84"
												href="${pageContext.request.contextPath }/redirect.do?page=djsf/all" />
											<area shape="rect" coords="429,88,534,128"
												href="${pageContext.request.contextPath }/redirect.do?page=djtf/all" />
											<area shape="rect" coords="455,152,565,201"
												href="${pageContext.request.contextPath }/redirect.do?page=bs/all" />
											<area shape="rect" coords="446,222,553,265" href="${pageContext.request.contextPath }/redirect.do?page=shour/all" />
											<area shape="rect" coords="412,277,522,320" href="${pageContext.request.contextPath }/redirect.do?page=dq/all" />
											<area shape="rect" coords="351,324,458,368" href="${pageContext.request.contextPath }/redirect.do?page=zhichu/all" />
											<area shape="rect" coords="285,373,390,399" href="${pageContext.request.contextPath }/redirect.do?page=zc/all" />
										</map>
									</div>
								</div>
							</div>
						</div>

					</div>




					<!-- 右边部分 -->
					<div class="subfiled-wrap">

						<div class="place">
							<span>位置：</span>
							<ul
								style="height: 40px; background: url(${pageContext.request.contextPath }/images/topright.jpg) repeat-x;">
								<li
									style="float: left; line-height: 40px; padding-left: 7px; padding-right: 12px;">
									最新政策</li>
							</ul>
						</div>

						<div class="subfiled-content hasBg dingdan">
							<ul class="welcome_list"></ul>
						</div>
					</div>

					<!-- 右边结束  -->
				</div>



			</div>


		</div>

	</div>
</body>

</html>
