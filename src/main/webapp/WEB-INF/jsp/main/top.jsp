<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
		<script src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
		<script src="${pageContext.request.contextPath}/layer/layer.js"></script>
	    <script type="text/javascript">
$(function() {
	//顶部导航切换
	$(".nav li a").click(function() {
		$(".nav li a.selected").removeClass("selected")
		$(this).addClass("selected");
	})
})

</script>


	</head>

	<body style="background: url(${pageContext.request.contextPath }/images/topbg.gif) repeat-x;">

		<div class="topleft">
			<img src="${pageContext.request.contextPath }/images/logo.png" title="系统首页" />
			
		</div>

		<ul class="nav">
			<li>
				<a href="/redirect.do?page=main/right" target="rightFrame" class="selected"><img
						src="${pageContext.request.contextPath }/images/icon01.png" title="工作台" />
					<h2>
						工作台
					</h2>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath }/sun/mysort/redirect.do?page=sort/all" target="rightFrame"><img
						src="${pageContext.request.contextPath }/images/icon02.png" title="模型管理" />
					<h2>
						房屋资料
					</h2>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath }/redirect.do?page=djrz/all" target="rightFrame"><img
						src="${pageContext.request.contextPath }/images/icon03.png" title="模块设计" />
					<h2>
						登记入住
					</h2>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath }/redirect.do?page=zsr/all1" target="rightFrame"><img
						src="${pageContext.request.contextPath }/images/icon04.png" title="常用工具" />
					<h2>
						房屋查询 
					</h2>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath }/redirect.do?page=zsr/all2" target="rightFrame"><img
						src="${pageContext.request.contextPath }/images/icon05.png" title="文件管理" />
					<h2>
						收入查询 
					</h2>
				</a>
			</li>
			<li>
				<a href="${pageContext.request.contextPath }/redirect.do?page=xx/ma" target="rightFrame"><img
						src="${pageContext.request.contextPath }/images/icon06.png" title="系统设置" />
					<h2>
						系统设置
					</h2>
				</a>
			</li>
		</ul>

		<div class="topright">
			<ul>
				<li>
					<span><img src="${pageContext.request.contextPath }/images/help.png" title="帮助" class="helpimg" />
					</span><a href="#" class="look">帮助</a>
				</li>
				<li>
					<a href="#" class="look">关于</a>
				</li>
				<li>
					<a href="${pageContext.request.contextPath }/sun/myemp/logout.do" id="myout" target="_parent">退出</a>
				</li>
			</ul>

			<div class="user">
				<span><%-- <%
				   Myemp emp=(Myemp)session.getAttribute("myuser");
				%><%=emp.getErealname() %> --%></span>
				<i>消息</i>
				<b>0</b>
			</div>

		</div>

	</body>
</html>

