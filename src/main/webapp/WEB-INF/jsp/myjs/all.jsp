<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/style.css"></link>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/font-awesome.min.css"></link>
<link href="${pageContext.request.contextPath }/css/bootstrap.min.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
<script src="${pageContext.request.contextPath}/layer/layer.js"></script>
<SCRIPT type="text/javascript">
	//////////////权限控制！！！//////////////
	var $webName = "${pageContext.request.contextPath}";
</SCRIPT>
<script src="${pageContext.request.contextPath}/js/myjs/myjs.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

	</head>
	<body>
		<!-- 位置信息 -->
		<div class="place">
			<span>位置：</span>
			<ul class="placeul">
				<li><a href="${pageContext.request.contextPath }/redirect.do?page=main/right">首页</a></li>
				<li><a href="${pageContext.request.contextPath }/redirect.do?page=myjs/all">角色管理</a></li>
				<li><a href="${pageContext.request.contextPath }/redirect.do?page=myjs/all">角色内容</a></li>
			</ul>
		</div>
		<!-- 内容表格信息 -->
		<div class="rightinfo">
			<div class="tools">
				<ul class="toolbar">
					<li class="addPart">
						<span><img src="${pageContext.request.contextPath }/images/t01.png" /> </span>添加
					</li>
				</ul>
				
			</div>
			<table class="tablelist"></table>
			<%-- 分页位置 --%>
			<div class="pagin">
				<div class="message"></div>
			</div>
		</div>
	</body>
</html>
