<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="${pageContext.request.contextPath }/css/style.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath }/css/select.css"
	rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
<script src="${pageContext.request.contextPath}/layer/layer.js"></script>
<script src="${pageContext.request.contextPath }/js/jquery.idTabs.min.js"></script>
<script src="${pageContext.request.contextPath }/js/select-ui.min.js"></script>
<script>
	var $webName = "${pageContext.request.contextPath}";
</script>
<%-- <script src="${pageContext.request.contextPath }/js/kindeditor.js"></script> --%>
<script src="${pageContext.request.contextPath }/js/kindeditor/kindeditor-all.js"></script>
<script src="${pageContext.request.contextPath }/js/kindeditor/zh-CN.js"></script>
<script type="text/javascript">
	/* KE.show({
		id : 'content7'
	});
	KindEditor.ready(function(K) {
		alert("dd")
		K.create("textarea[name='content7']", {
			themeType : "simple",
			resizeType : 1,
			afterBlur : function() {
				this.sync();
			}
		});

	}); */
	//简单模式初始化
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content7"]', {
			resizeType : 1,
			allowPreviewEmoticons : false,
			allowImageUpload : false,
			cssPath : $webName + '/js/kindeditor/themes/simple/simple.css',
			afterCreate : function() {
				this.sync();
			},
			afterBlur : function() {
				this.sync();
			}
		});
	});
</script>

<SCRIPT type="text/javascript">
	function ck() {
		var ctitle = $(".dfinput").val();
		if (ctitle.length == 0) {
			layer.tips('标题不能为空！', '.dfinput', {
				tips : [ 2, 'red' ]
			});
			$(".dfinput").focus();
			return false;
		} else {
			return true;
		}

	}
</SCRIPT>
<script src="${pageContext.request.contextPath }/js/zc/zcadd.js"></script>
<STYLE type="text/css">
.forminfo1 li {
	margin-bottom: 15px;
	clear: both;
}
</STYLE>
</head>
<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="#">首页</a></li>
			<li><a href="#">表单</a></li>
		</ul>
	</div>

	<div class="formbody">

		<div class="formtitle">
			<span>&nbsp;&nbsp;&nbsp;&nbsp;政策信息</span>
		</div>

		<ul class="forminfo">
			<li><label> 文章标题 </label> <input name="ctitle" id="ctitle"
				type="text" class="dfinput" /></li>
			<li><textarea id="content7" name="content7"
					style="width: 720px; height: 230px; visibility: hidden;"></textarea>
			</li>
			<li><label> &nbsp; </label>
				<button name="" class="btn" onclick="return ck();">确认保存</button></li>
		</ul>
	</div>
</body>
</html>
