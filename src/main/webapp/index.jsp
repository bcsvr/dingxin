<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>登录页</title>
		<link href="${pageContext.request.contextPath }/css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/layer/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/index/myindex.js"></script>
	</head>

	<body
		style="background-color: #df7611; background-image: url(images/light.png); background-repeat: no-repeat; background-position: center top; overflow: hidden;">

		<div id="mainBody">
			<div id="cloud1" class="cloud"></div>
			<div id="cloud2" class="cloud"></div>
		</div>

		<div class="logintop">
			<span>欢迎使用鼎鑫出租房管理平台</span>
			<ul>
				<li>
					<a href="#">回首页</a>
				</li>
				<li>
					<a href="#" class="look">帮助</a>
				</li>
				<li>
					<a href="#" class="look">关于</a>
				</li>
			</ul>
		</div>

		<div class="loginbody">

			<span class="systemlogo"></span>
			<form action="./login" method="post" theme="simple">
				<div class="loginbox">
					<ul>
						<li>
							<input name="ename" type="text" class="loginuser"/>
						</li>
						<li>
							<input name="epsw" type="password" class="loginpwd"/>
						</li>
						<li>
							<input name="" type="button" class="loginbtn" value="登录" />
							<label>
								&nbsp;
							</label>
							<label>
								<a href="#" class="look">忘记密码？</a>
							</label>
						</li>
					</ul>
				</div>

			</form>
		</div>

		<div class="loginbm">
			版权所有 2020 湖南万树IT科技有限公司
		</div>
	</body>
</html>
