var hid = 0;
var aid = 0;
$(function() {

	init();
	//数据验证
	myaddSe();
	//提交表单位
	commitItem();
});
function init() {
	$("#dkd").focus();

	var bid = $("#bid").val();
	//查询基本信息
	$.ajax( {
		url : $webName + '/biao/findBiaoByBid.do',
		data : {
			bid : bid
		},
		dataType : 'json',
		type : 'post',
		success : function(mydata) {
			$("#dkd").val(mydata.map.byBid.dkd);
			$("#skd").val(mydata.map.byBid.skd);
			$("#mkd").val(mydata.map.byBid.mkd);

			hid = mydata.map.byBid.myhouse.hid;
			aid = mydata.map.byBid.myhouse.aid;
			
			/**************得到下拉框的值 *************/
			$.ajax( {
				url : $webName + '/sun/myarea/area/findAllAreaPageData.do',
				dataType : 'json',
				type : 'post',
				data : '',
				async : true,
				success : function(mydata) {
					$.each(mydata.map.pageInfo.list, function(index, xx) {
						if (aid == xx.aid) {
							$("#aid").append(
									"<option value=" + xx.aid
											+ " selected='selected'>"
											+ xx.aname + "</option>");
						} else {
							$("#aid").append(
									"<option value=" + xx.aid + ">" + xx.aname
											+ "</option>");
						}
					});
				}
			});
			/*****************************/

			/**************得到房子信息***********/
			$.ajax( {
				url : $webName + '/house/findAllHousePD.do',
				dataType : 'json',
				type : 'post',
				data : {
					aid : aid
				},
				async : true,
				success : function(mydata) {
					$.each(mydata.map.pageInfo.list, function(index, xx) {
						if (hid == xx.hid) {
							$("#hid").append(
									"<option value=" + xx.hid
											+ " selected='selected'>"
											+ xx.haddress + "</option>");
						} else {
							$("#hid").append(
									"<option value=" + xx.hid + ">"
											+ xx.haddress + "</option>");
						}

					});
				}
			});
			/*****************************/
		}
	});

};
/*****************************************/

function myaddSe() {
	$("#aid").bind(
			"change",
			function() {
				var aid = $(this).val();
				$("#hid").empty();
				if (aid != 0) {

					$.ajax( {
						url :  $webName + '/house/findAllHouse.do',
						dataType : 'json',
						type : 'post',
						data : {
							aid : aid
						},
						async : true,
						success : function(mydata) {
							$.each(mydata, function(index, xx) {
								$("#hid").append(
										"<option value=" + xx.hid + ">"
												+ xx.haddress + "</option>");
							});
						}
					});
				} else {
					$("#hid").append("<option value=0>---请选择房屋---</option>");
				}

			});
}


/******************************提交表单********************************/
function commitItem() {
    $(".btn").bind("click",function(){
	var hid = $("#hid").val();
	var dkd = $("#dkd").val();
	var skd = $("#skd").val();
	var mkd = $("#mkd").val();
	var bid=$("#bid").val();
	
	var myage=/^[0-9]*$/;   //正则表达式
	var m=myage.test(dkd);
	var n=myage.test(skd);
	var k=myage.test(mkd);
	
	
	if(hid.length==0)
		{
		   layer.tips('房子名称不能为空！','#hid',{tips:[2,'red']});
		   $("#hid").focus();
		   return false;
		}
	if(hid==0)
		{
		   layer.tips('房子名称不能为空！','#hid',{tips:[2,'red']});
		   $("#hid").focus();
		   return false;
		}
	else if(dkd.length==0)
		{
		  layer.tips('电表刻度不能为空！','#dkd',{tips:[2,'red']});
		  $("#dkd").focus();
		  return false;
		}
	else if(!m)
		{
		  layer.tips('电表刻度只能是正数！','#dkd',{tips:[2,'red']});
		  $("#dkd").focus();
		  return false;
		}
	else if(skd.length==0)
		{
		  layer.tips('水表刻度不能为空！','#skd',{tips:[2,'red']});
		  $("#skd").focus();
		  return false;
		}
	else if(!n)
		{
		  layer.tips('水表刻度只能是正数！','#skd',{tips:[2,'red']});
		  $("#skd").focus();
		  return false;
		}
	else if(mkd.length==0)
		{
		  layer.tips('煤气表刻度不能为空！','#mkd',{tips:[2,'red']});
		  $("#mkd").focus();
		  return false;
		}
	else if(!k)
		{
		  layer.tips('煤气表刻度只能是正数！','#mkd',{tips:[2,'red']});
		  $("#mkd").focus();
		  return false;
		}
	else
		{
		   var mypart = "skd="+skd+"&dkd="+dkd+"&mkd="+mkd+"&hid="+hid+"&bid="+bid;
		   var i = layer.load(0);
		   $.post( $webName + '/biao/updateMybiao.do',mypart,function(mydata){
			 layer.close(i);
			 if(mydata.code==200)
				 {
				   parent.layer.msg('修改成功！', {icon : 6,time : 3000});
				   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
				   parent.layer.close(index);
				 }
			 else
				 {
				     parent.layer.msg('修改失败', 2, 9);
				 }
		   },'json');
		}
	});
}