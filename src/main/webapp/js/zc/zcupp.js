var pid = 0;
var jid = 0;

$(function() {
	init();
	//数据验证
	//checkItem();
	//提交表单位
	commitItem();
});
function init() {
	var cid = $("#cid").val();
	$.ajax({
		url : $webName + '/zc/findZcByCid.do',
		data : {
			cid : cid
		},
		dataType : 'json',
		type : 'post',
		success : function(mydata) {
			$("#ctitle").val(mydata.map.list.ctitle);
			KindEditor.html("#content7", mydata.map.list.cremark);
			$("#cid").val(mydata.map.list.cid);
		}
	});
}
;
/*****************************************/
function commitItem() {
	$(".btn").bind("click", function() {
		var ctitle = $("#ctitle").val();
		var cremark = $("#content7").val();
		alert(cremark);
		var cid = $("#cid").val();
		if ($("#ctitle").val().length == 0) {
			layer.tips('标题不能为空！', '#ctitle', {
				tips : [ 2, 'red' ]
			});
			$("#ctitle").focus();
			return false;
		} else if ($("#content7").val().length == 0) {
			layer.tips('内容不能为空！', '#txt', {
				tips : [ 2, 'red' ]
			});
			$("#txt").focus();
			return false;
		} else {
			var mypart = "ctitle=" + ctitle + "&cremark=" + cremark + "&cid=" + cid;
			var i = layer.load(0);
			$.post( $webName + '/zc/updateZcByCid.do', mypart, function(mydata) {
				layer.close(i);
				if (mydata.code == 200) {

					parent.layer.msg('修改成功！', {
						icon : 6,
						time : 3000
					});
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
					parent.layer.close(index);
				} else {
					parent.layer.msg('修改失败', 2, 9);
				}
			}, 'json');
		}
	});
}