$(function(){
	init();
	//数据验证
	//checkItem();
	//提交表单位
	commitItem();
});
/****************获得焦点********************/
function init() {
	$("#haddress").focus();
	
	//得到下拉框的值 
	$.ajax({
		url:$webName+'/sun/mysort/sort/findAllSort.do',
		dataType:'json',
		type:'post',
		data:'',
		async : true,
		success:function(mydata) {
		   $.each(mydata.map.pageInfo.list,function(index,xx){
			   $("#sid").append("<option value="+xx.sid+">"+xx.sname+"</option>");
		   });
		}
	});
	
	$.ajax({
		url:$webName+'/sun/myarea/area/findAllAreaPageData.do',
		dataType:'json',
		type:'post',
		data:'',
		async : true,
		success:function(mydata) {
			$.each(mydata.map.pageInfo.list,function(index,xx){
				$("#aid").append("<option value="+xx.aid+">"+xx.aname+"</option>");
		   	});
		}
	});
};


/******************************提交表单********************************/
function commitItem(){
	$(".btn").bind("click",function(){
		var sid = $("#sid").val();
		var aid = $("#aid").val();
		var haddress = $("#haddress").val();
		var hfh = $("#hfh").val();
		var hhx = $("#hhx").val();
		var hmj = $("#hmj").val();
		var hcx = $("#hcx").val();
		var hmoney = $("#hmoney").val();
		var hwf = $("#hwf").val();
		var hdx = $("#hdx").val();
		var hsf = $("#hsf").val();
		var hmq = $("#hmq").val();
		var dkd = $("#dkd").val();
		var skd = $("#skd").val();
		var mkd = $("#mkd").val();
		var hjp = $("#hjp").val();
		var hremark = $("#hremark").val();
		var files=new Array();
		files.push('file1');
		files.push('file2');
		files.push('file3');
		
			$.ajaxFileUpload({
			    url:$webName+'/house/addHouse.do',
			    secureuri:false,//一般设置为false
			    fileElementId:files,//上传对象
			    data:{
				  "sid":sid,
				  "aid":aid,
				  "haddress":haddress,
				  "hfh":hfh,
				  "hhx":hhx,
				  "hmj":hmj,
				  "hcx":hcx,
				  "hmoney":hmoney,
				  "hwf":hwf,
				  "hdx":hdx,
				  "hsf":hsf,
				  "hmq":hmq,
				  "dkd":dkd,
				  "skd":skd,
				  "mkd":mkd,
				  "hjp":hjp,
				  "hremark":hremark
				}, //上传控件以外的控件对应的参数
				secureuri : false, // 是否启用安全提交，默认为false
				dataType : 'json', // 服务器返回的格式
				async : false,
			    dataType: 'json', 
			    success : function(mydata, status) {
					layer.msg('添加成功！', {
						icon : 6,
						time : 3000
					});
					var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
					parent.layer.close(index);
				},
				error : function(data, status, e) {
					parent.layer.msg('添加成功!', {
						icon : 1
					});
					var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
					parent.layer.close(index);
				}
			    
			});
		
	});
}