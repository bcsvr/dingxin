$(function() {

	init();
	//数据验证
	// checkItem();
	//提交表单位
	commitItem();
});
function init() {
	var zid = $("#zid").val();
	$.ajax({
		url : $webName + '/zhichu/findZhichuByZid.do',
		data : {
			zid : zid
		},
		dataType : 'json',
		type : 'post',
		success : function(mydata) {
			$("#zmoney").val(mydata.map.list.zmoney);
			$("#zremark").val(mydata.map.list.zremark);
			var t = mydata.map.list.ztm;
			if (t == "客户房租") {
				$('#ztm option:eq(0)').attr('selected', true);
			} else if (t == "房屋维修") {
				$('#ztm option:eq(1)').attr('selected', true);
			} else {
				$('#ztm option:eq(2)').attr('selected', true);
			}
		}
	});

}
;
/*****************************************/

/******************************提交表单********************************/
function commitItem() {
	$(".btn").bind("click", function() {
		var zmoney = $("#zmoney").val();
		var ztm = $("#ztm").val();
		var zid = $("#zid").val();
		var zremark = $("#zremark").val();
		var myage = /^[0-9]*$/; //正则表达式
		var m = myage.test(zmoney);


		if (zmoney.length == 0) {
			layer.tips('收入金额不能为空！', '#zmoney', {
				tips : [ 2, 'red' ]
			});
			$("#zmoney").focus();
			return false;
		} else if (!m) {
			layer.tips('收入金额只能为正数！', '#zmoney', {
				tips : [ 2, 'red' ]
			});
			$("#zmoney").focus();
			return false;
		} else if (zremark.length == 0) {
			layer.tips('收入备注不能为空！', '#zremark', {
				tips : [ 2, 'red' ]
			});
			$("#zremark").focus();
			return false;
		} else {
			var mypart = "zmoney=" + zmoney + "&ztm=" + ztm + "&zremark=" + zremark + "&zid=" + zid;
			var i = layer.load(0);
			$.post($webName + '/zhichu/updateZhichu.do', mypart, function(mydata) {
				layer.close(i);
				if (mydata.code == 200) {
					parent.layer.msg('修改成功！', {
						icon : 6,
						time : 3000
					});
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
					parent.layer.close(index);
				} else {
					parent.layer.msg('修改失败', 2, 9);
				}
			}, 'json');
		}
	});
}