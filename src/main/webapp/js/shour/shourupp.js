$(function() {

	init();
	//数据验证
	// checkItem();
	//提交表单位
	commitItem();
});
function init() {
	var sid = $("#sid").val();
	$.ajax({
		url : $webName + '/shouru/findShouruBySid.do',
		data : {
			sid : sid
		},
		dataType : 'json',
		type : 'post',
		success : function(mydata) {
			$("#smoney").val(mydata.map.list.smoney);
			$("#sremark").val(mydata.map.list.sremark);
			var t = mydata.map.list.stm;
			if (t == "客户房租") {
				$('#stm option:eq(0)').attr('selected', true);
			} else if (t == "客户物损赔偿") {
				$('#stm option:eq(1)').attr('selected', true);
			} else {
				$('#stm option:eq(2)').attr('selected', true);
			}
		}
	});

}
;
/*****************************************/


/******************************提交表单********************************/
function commitItem() {
	$(".btn").bind("click", function() {
		var smoney = $("#smoney").val();
		var stm = $("#stm").val();
		var sid = $("#sid").val();
		var sremark = $("#sremark").val();
		var myage = /^[0-9]*$/; //正则表达式
		var m = myage.test(smoney);
		if (smoney.length == 0) {
			layer.tips('收入金额不能为空！', '#smoney', {
				tips : [ 2, 'red' ]
			});
			$("#smoney").focus();
			return false;
		} else if (!m) {
			layer.tips('收入金额只能为正数！', '#smoney', {
				tips : [ 2, 'red' ]
			});
			$("#smoney").focus();
			return false;
		} else if (sremark.length == 0) {
			layer.tips('收入备注不能为空！', '#sremark', {
				tips : [ 2, 'red' ]
			});
			$("#sremark").focus();
			return false;
		} else {
			var mypart = "smoney=" + smoney + "&stm=" + stm + "&sremark=" + sremark + "&sid=" + sid;
			var i = layer.load(0);
			$.post( $webName + '/shouru/updateShouru.do', mypart, function(mydata) {
				layer.close(i);
				if (mydata.code == 200) {
					parent.layer.msg('修改成功！', {
						icon : 6,
						time : 3000
					});
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
					parent.layer.close(index);
				} else {
					parent.layer.msg('修改失败', 2, 9);
				}
			}, 'json');
		}
	});
}