$(function(){
 
  init();
  //数据验证
  checkItem();
  //提交表单位
   commitItem();
});
function init() {
	var aid=$("#aid").val(); 
	$.ajax({
		url:$webName+'/sun/myarea/area/getArea.do',
		data:{aname:null,aid:aid},
		dataType:'json',
		type:'post',
		success:function(mydata)
		{
			$("#aname").val(mydata.map.area.aname);
			
			$("#aid").val(mydata.map.area.aid);
		}
	});
	
};
/*****************************************/
/******************失去焦点事件****************************/
function checkItem()
{
	$("#aname").focusout(function(){
		var aname=$("#aname").val();
		if(aname.length==0)
			{
			   layer.tips('区域名称不能为空！','#aname',{tips:[2,'red']});
			}
		else
			{
			   $.ajax({
				   url:$webName+'/sun/myarea/area/getArea.do',
				   dataType:'json',
				   type:'post',
				   data:{aname:aname,aid:null},
				   async : true,
				   success:function(mydata)
				   {
					   if(mydata.code==500)
						   {
						      $("#aname").addClass("newsuccess");
					          $("#aname").removeClass("newerror");
						   }
					   else
						   {
						       layer.tips('对不起区域已存在！','#aname',{tips:[2,'red']});
						   }
					   $("#botao").val(mydata);
				   }
			   });
			}
	});
}



/******************************提交表单********************************/
function commitItem()
{
	$(".btn").bind("click",function(){	
	var aname = $("#aname").val();
	var aid=$("#aid").val();
	if(aname.length==0)
		{
		   layer.tips('角色名称不能为空！','#aname',{tips:[2,'red']});
		   $("#aname").focus();
		   return false;
		}
	else if($("#botao").val()>0)
		{
		  layer.tips('对不起角色已存在！','#aname',{tips:[2,'red']});
		  $("#aname").focus();
		  return false;
		}
	else
		{
		   var myarea = "aname=" + aname + "&aid="+aid;
		   var i = layer.load(0);
		   $.post($webName+'/sun/myarea/area/uppArea.do',myarea,function(mydata){
			 layer.close(i);
			 if(mydata.code==200)
				 {
				   parent.layer.msg('修改成功！', {icon : 6,time : 3000});
				   var index = parent.layer.getFrameIndex(window.name); //获取窗口索引(真正的关 )
				   parent.layer.close(index);
				 }
			 else
				 {
				     parent.layer.msg('修改失败', 2, 9);
				 }
		   },'json');
		}
	});
}