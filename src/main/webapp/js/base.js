function formatLongToDate(datelong, flag) {
	var dateTypeDate = "";
	var date = new Date();
	date.setTime(datelong);
	dateTypeDate += date.getFullYear(); //年  
	if(flag==-1){
		dateTypeDate += "-" + date.getMonth(); //月  
		dateTypeDate += "-" + date.getDay(); //日  
	}else{
		dateTypeDate += "年" + date.getMonth(); //月  
		dateTypeDate += "月" + date.getDay(); //日  
		dateTypeDate += "日 " 
	}
	
	return dateTypeDate;
}