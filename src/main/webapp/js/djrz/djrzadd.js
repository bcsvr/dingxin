$(function() {
	init();

	// 下拉框联动事件
	myaddSe();

	// 提交表单位
	commitItem();
});
/** **************获得焦点同时得到两个下拉框的内容******************* */
function init() {
	$("#myj").focus();
	// 得到下拉框的值
	$.ajax({
		url : $webName + '/custom/findAllCustom.do',
		dataType : 'json',
		type : 'post',
		data : '',
		async : true,
		success : function(mydata) {
			$.each(mydata.map.pageInfo.list, function(index, xx) {
				$("#cid").append(
						"<option value=" + xx.cid + ">" + xx.cname
								+ "</option>");
			});
		}
	});

	$.ajax({
		url : $webName + '/sun/myarea/area/findAllAreaPageData.do',
		dataType : 'json',
		type : 'post',
		data : '',
		async : true,
		success : function(mydata) {

			$.each(mydata.map.pageInfo.list, function(index, xx) {
				$("#aid").append(
						"<option value=" + xx.aid + ">" + xx.aname
								+ "</option>");
			});
		}
	});
};

function myaddSe() {
	$("#aid").bind(
			"change",
			function() {
				var aid = $(this).val();
				$("#hid").empty();
				if (aid != 0) {
					$.ajax({
						url : $webName + '/house/findAllHousePD.do',
						dataType : 'json',
						type : 'post',
						data : {
							aid : aid
						},
						async : true,
						success : function(mydata) {
							$.each(mydata.map.pageInfo.list, function(index, xx) {
								$("#hid").append(
										"<option value=" + xx.hid + ">" + xx.haddress + " " + xx.hfh + "</option>");
							});
						}
					});
				} else {
					$("#hid").append("<option value=0>---请选择房屋---</option>");
				}

			});
}
/** ************************************************************************* */

/** ****************************提交表单******************************* */
function commitItem() {
	$(".btn").bind("click", function() {
		var cid = $("#cid").val();
		var hid = $("#hid").val();
		var myj = $("#myj").val();
		var myzj = $("#myzj").val();
		var time = $("#time").val();
//		var f = $("#file").val();
		var file=new Array();
		file.push('file');
		
		var myage = /^[0-9]*$/; // 正则表达式
		var m = myage.test(myj);
		var n = myage.test(myzj);
		if (hid == 0) {
			layer.tips('请选择房屋！', '#hid', {
				tips : [ 2, 'red' ]
			});
			$("#hid").focus();
			return false;
		} else if (myj.length == 0) {
			layer.tips('出租押金不能为空！', '#myj', {
				tips : [ 2, 'red' ]
			});
			$("#myj").focus();
			return false;
		} else if (!m) {
			layer.tips('出租押金只能为正数！', '#myj', {
				tips : [ 2, 'red' ]
			});
			$("#myj").focus();
			return false;
		} else if (myzj.length == 0) {
			layer.tips('预收租金不能为空！', '#myzj', {
				tips : [ 2, 'red' ]
			});
			$("#myzj").focus();
			return false;
		} else if (!n) {
			layer.tips('预收租金只能为正数！', '#myzj', {
				tips : [ 2, 'red' ]
			});
			$("#myzj").focus();
			return false;
		} else if (mbegintime.length == 0) {
			layer.tips('时间不能为空！', '#time', {
				tips : [ 2, 'red' ]
			});
			$("#mbegintime").focus();
			return false;
		} else if (file.length == 0) {
			layer.tips('合同不能为空！', '#doc', {
				tips : [ 2, 'red' ]
			});
			$("#file").focus();
			return false;
		} else {
			
			$.ajaxFileUpload({
				url : $webName + '/djrz/addDj.do',
				data : {
					cid : $("#cid").val(),
					hid : $("#hid").val(),
					myj : $("#myj").val(),
					myzj : $("#myzj").val(),
					mbegintime : $("#mbegintime").val()
				}, // 要传到后台的参数，没有可以不写
				secureuri : false, // 是否启用安全提交，默认为false
				fileElementId : file, // 文件选择框的id属性
				dataType : 'json', // 服务器返回的格式
				async : false,
				success : function(mydata, status) {
					layer.msg('添加成功！', {
						icon : 6,
						time : 3000
					});
					var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
					parent.layer.close(index);
				},
				error : function(data, status, e) {
					parent.layer.msg('添加成功!', {
						icon : 1
					});
					var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
					parent.layer.close(index);
				}
			});
		}
	});
}