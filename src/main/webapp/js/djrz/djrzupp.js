var cid = 0;
var hid = 0;
var mid = 0;
var aid = 0;

$(function() {
	init();
	//数据验证
	//checkItem();
	//提交表单位
	myaddSe();
	commitItem();
});


function init() {
	$("#myj").focus();

	mid = $("#mid").val();
	//--------------------------------------------------
	$.ajax({
		url : $webName + '/djrz/findDjByMid.do',
		dataType : 'json',
		type : 'post',
		data : {
			mid : mid
		},
		async : true,
		success : function(mydata) {
			//$("#cid").val(mydata.map.djByMid.mycus.cname);
			$("#myj").val(mydata.map.djByMid.myj);
			$("#myzj").val(mydata.map.djByMid.myzj);
			$("#time").val(mydata.map.djByMid.mbegintime);
			
			$("#mid").val(mydata.map.djByMid.mid);
			$("#xaid").val(mydata.map.djByMid.aid);
			var cid = mydata.map.djByMid.cid;
			var hid = mydata.map.djByMid.hid;
			var aid = mydata.map.djByMid.myhouse.aid;

			/****************************客户信息***************************/
			$.ajax({
				url : $webName + '/custom/findAllCustom.do',
				dataType : 'json',
				type : 'post',
				data : '',
				async : true,
				success : function(mydata) {
					$.each(mydata.map.pageInfo.list, function(index, xx) {
						if (cid == xx.cid) {
							$("#cid").append("<option value=" + xx.cid + " selected='selected'>" + xx.cname + "</option>");
						} else {
							$("#cid").append("<option value=" + xx.cid + ">" + xx.cname + "</option>");
						}

					});
				}
			});

			/********************************片区下拉列表*****************************/
			$.ajax({
				url : $webName + '/sun/myarea/area/findAllAreaPageData.do',
				dataType : 'json',
				type : 'post',
				data : '',
				async : true,
				success : function(mydata) {
					$.each(mydata.map.pageInfo.list, function(index, xx) {
						if (aid == xx.aid) {
							$("#aid").append("<option value=" + xx.aid + " selected='selected'>" + xx.aname + "</option>");
						} else {
							$("#aid").append("<option value=" + xx.aid + ">" + xx.aname + "</option>");
						}
					});
				}
			});


			/********************************房子信息***************************/
			$.ajax({
				url : $webName + '/house/findAllHousePD.do',
				dataType : 'json',
				type : 'post',
				data : {
					aid : aid
				},
				async : true,
				success : function(mydata) {

					$.each(mydata.map.pageInfo.list, function(index, xx) {
						if (hid == xx.hid) {
							$("#hid").append("<option value=" + xx.hid + " selected='selected'>" + xx.haddress + " " + xx.hfh + "</option>");
						} else {
							$("#hid").append("<option value=" + xx.hid + ">" + xx.haddress + " " + xx.hfh + "</option>");
						}


					});
				}
			});
		/***************************************************/
		}
	});
}
;

/****************************联动下拉:修改片区时，房屋跟着变化********************************/
function myaddSe() {
	$("#aid").bind("change", function() {
		var aid = $(this).val();
		$("#hid").empty();
		if (aid != 0) {
			$.ajax({
				url : $webName + '/house/findAllHouse.do',
				dataType : 'json',
				type : 'post',
				data : {
					aid : aid
				},
				async : true,
				success : function(mydata) {
					$.each(mydata.map.pageInfo.list, function(index, xx) {
						$("#hid").append("<option value=" + xx.hid + ">" + xx.haddress + " " + xx.hfh + "</option>");
					});
				}
			});
		} else {
			$("#hid").append("<option value=0>---请选择房屋---</option>");
		}

	});
}
/***************************************************************************************************/


/******************************提交表单********************************/
function commitItem() {
	$(".btn").bind("click", function() {
		//修改登记，如果要换租房子的人，相当于转租，首先需要登记客户，然后再是修改，修改可以改成下拉列表
		var cid = $("#cid").val();
		//修改房屋ID，代表把租的房子改成另外一栋，如果另外一栋已经被租，则会产生问题
		var hid = $("#hid").val();
		var myj = $("#myj").val();
		var myzj = $("#myzj").val();
		var time = $("#time").val();
		var file = $("#file").val();
		var mid = $("#mid").val();

		var myage = /^[0-9]*$/; //正则表达式
		var m = myage.test(myj);
		var n = myage.test(myzj);
		if (hid == 0) {
			layer.tips('请选择房屋！', '#hid', {
				tips : [ 2, 'red' ]
			});
			$("#hid").focus();
			return false;
		} else if (myj.length == 0) {
			layer.tips('出租押金不能为空！', '#myj', {
				tips : [ 2, 'red' ]
			});
			$("#myj").focus();
			return false;
		} else if (!m) {
			layer.tips('出租押金只能为正数！', '#myj', {
				tips : [ 2, 'red' ]
			});
			$("#myj").focus();
			return false;
		} else if (myzj.length == 0) {
			layer.tips('预收租金不能为空！', '#myzj', {
				tips : [ 2, 'red' ]
			});
			$("#myzj").focus();
			return false;
		} else if (!n) {
			layer.tips('预收租金只能为正数！', '#myzj', {
				tips : [ 2, 'red' ]
			});
			$("#myzj").focus();
			return false;
		} else if (time.length == 0) {
			layer.tips('时间不能为空！', '#time', {
				tips : [ 2, 'red' ]
			});
			$("#time").focus();
			return false;
		} else {
			$.ajaxFileUpload({
				url :  $webName + '/djrz/updateDj.do',
				secureuri : false, //一般设置为false
				fileElementId : [ 'file' ], //上传对象 
				data : {
					"mbegintime" : $("#time").val(),
					"cid" : cid,
					"hid" : hid,
					"myj" : myj,
					"myzj" : myzj,
					"mid" : $("#mid").val()
				}, //上传控件以外的控件对应的参数
				dataType : 'json',
				success : function(mydata, status) {
					parent.layer.msg('修改成功!!', {
						icon : 1
					});
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
					parent.layer.close(index);
				},
				error : function(data, status, e) //服务器响应失败处理函数
				{
					parent.layer.msg('修改成功!!', {
						icon : 1
					});
					var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
					parent.layer.close(index);

				}
			});
		}
	});
}