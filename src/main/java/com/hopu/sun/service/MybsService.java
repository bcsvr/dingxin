package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Mybs;

import java.util.List;


public interface MybsService extends IService<Mybs> {
    //查询
    List<Mybs> findAllBs();

    //添加
    int addBs(Mybs entity);

    //修改
    int updateBs(Mybs entity);

    //根据ID查询
    Mybs findBsByBid(Mybs entity);

}
