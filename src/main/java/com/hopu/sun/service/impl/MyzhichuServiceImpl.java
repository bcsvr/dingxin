package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MyzhichuMapper;
import com.hopu.sun.model.Myzhichu;
import com.hopu.sun.service.MyzhichuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyzhichuServiceImpl extends ServiceImpl<MyzhichuMapper, Myzhichu> implements MyzhichuService {
    @Autowired
    private MyzhichuMapper myzhichuMapper;

    //查询所有
    @Override
    public List<Myzhichu> findAllzhichu() {
        return myzhichuMapper.findAllzhichu();
    }

    //添加
    @Override
    public int addZhichu(Myzhichu entity) {
        return myzhichuMapper.addZhichu(entity);
    }

    //根据ID查询
    @Override
    public Myzhichu findZhichuByZid(Myzhichu entity) {
        return myzhichuMapper.findZhichuByZid(entity);
    }

    //修改
    @Override
    public int updateZhichu(Myzhichu entity) {
        return myzhichuMapper.updateZhichu(entity);
    }
}
