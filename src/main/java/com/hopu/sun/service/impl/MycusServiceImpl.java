package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MycusMapper;
import com.hopu.sun.model.Mycus;
import com.hopu.sun.service.MycusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MycusServiceImpl extends ServiceImpl<MycusMapper, Mycus> implements MycusService {
    @Autowired
    private MycusMapper mycusMapper;

    //查询
    @Override
    public List<Mycus> findAllCustom() {
        return mycusMapper.findAllCustom();
    }

    //添加
    @Override
    public int addPart(Mycus entity) {
        return mycusMapper.addCus(entity);
    }

    //修改
    @Override
    public int uppCus(Mycus entity) {
        return mycusMapper.uppCus(entity);
    }

    //根据ID查询
    @Override
    public Mycus findCusById(Mycus entity) {
        return mycusMapper.findCusById(entity);
    }
}
