package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MyhtMapper;
import com.hopu.sun.model.Myht;
import com.hopu.sun.service.MyhtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyhtServiceImpl extends ServiceImpl<MyhtMapper, Myht> implements MyhtService {
    @Autowired
    private MyhtMapper myhtMapper;

    //查询
    @Override
    public List<Myht> findAllht() {
        return myhtMapper.findAllht();
    }

    //添加
    @Override
    public int addHt(Myht entity) {
        return myhtMapper.addHt(entity);
    }

    //根据ID查询
    @Override
    public Myht findByHid(Myht entity) {
        return myhtMapper.findByHid(entity);
    }

    //修改
    @Override
    public int uppHt(Myht entity) {
        return myhtMapper.uppHt(entity);
    }

}
