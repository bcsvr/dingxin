package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MyhouseMapper;
import com.hopu.sun.model.Myhouse;
import com.hopu.sun.service.MyhouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyhouseServiceImpl extends ServiceImpl<MyhouseMapper, Myhouse> implements MyhouseService {
    @Autowired
    private MyhouseMapper myhouseMapper;

    //查询所有
    @Override
    public List<Myhouse> findAllHousePD() {
        return myhouseMapper.findAllHousePD();
    }

    //根据ID查询
    @Override
    public Myhouse findHouseById(Myhouse entity) {
        return myhouseMapper.findHouseById(entity);
    }

    //添加
    @Override
    public int addHouse(Myhouse entity) {
        return myhouseMapper.addHouse(entity);
    }

    //修改
    @Override
    public int uppHouse(Myhouse entity) {
        return myhouseMapper.uppHouse(entity);
    }

    //修改状态
    @Override
    public int updatehouse(Myhouse entity) {
        return myhouseMapper.updatehouse(entity);
    }

    //条件查询
    @Override
    public List<Myhouse> findAllHouseByCondition(Myhouse entity) {
        return myhouseMapper.findAllHouseByCondition(entity);
    }

    //ID查询
    @Override
    public List<Myhouse> findAllHouseByAidAndSid(Myhouse entity) {
        return myhouseMapper.findAllHouseByAidAndSid(entity);
    }
}
