package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MysortMapper;
import com.hopu.sun.model.Mysort;
import com.hopu.sun.service.MysortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MysortServiceImpl extends ServiceImpl<MysortMapper, Mysort> implements MysortService {
    @Autowired
    private MysortMapper mysortMapper;

    //查询
    @Override
    public List<Mysort> findMysort() {

        return mysortMapper.findMysort();
    }

    //添加
    @Override
    public int addSort(Mysort entity) {
        return mysortMapper.addSort(entity);
    }

    //修改
    @Override
    public int uppSort(Mysort entity) {
        return mysortMapper.uppSort(entity);
    }

    //根据条件查询
    @Override
    public Mysort findSortByNameOrId(Mysort entity) {
        return mysortMapper.findSortByNameOrId(entity);
    }

}
