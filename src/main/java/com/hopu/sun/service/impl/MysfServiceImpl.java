package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MysfMapper;
import com.hopu.sun.model.Mysf;
import com.hopu.sun.service.MysfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MysfServiceImpl extends ServiceImpl<MysfMapper, Mysf> implements MysfService {
    @Autowired
    private MysfMapper mysfMapper;

    //查询
    @Override
    public List<Mysf> findAllDjsf(Mysf entity) {
        return mysfMapper.findAllDjsf(entity);
    }

    //添加
    @Override
    public int addSfXf(Mysf entity) {
        return mysfMapper.addSfXf(entity);
    }

    //根据ID查询
    @Override
    public List<Mysf> findAllSfByMid(Mysf entity) {
        return mysfMapper.findAllSfByMid(entity);
    }
}
