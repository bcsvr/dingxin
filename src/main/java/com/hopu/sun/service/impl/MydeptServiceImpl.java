package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MydeptMapper;
import com.hopu.sun.model.Mydept;
import com.hopu.sun.service.MydeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MydeptServiceImpl extends ServiceImpl<MydeptMapper, Mydept> implements MydeptService {
    @Autowired
    MydeptMapper mydeptMapper;

    //查询所有部门
    @Override
    public List<Mydept> findAllDept(Mydept entity) {
        return mydeptMapper.findAllDept(entity);
    }

    //添加
    public int addPart(Mydept entity) {
        return mydeptMapper.addPart(entity);
    }

    //修改
    public int updatePart(Mydept entity) {
        return mydeptMapper.updatePart(entity);
    }

    //根据条件查询
    public Mydept findPart(Mydept entity) {
        return mydeptMapper.findPart(entity);
    }

}
