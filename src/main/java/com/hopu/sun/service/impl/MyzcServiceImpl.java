package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MyzcMapper;
import com.hopu.sun.model.Myzc;
import com.hopu.sun.service.MyzcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyzcServiceImpl extends ServiceImpl<MyzcMapper, Myzc> implements MyzcService {
    @Autowired
    private MyzcMapper myzcMapper;

    //查询所有
    @Override
    public List<Myzc> findAllZc() {
        return myzcMapper.findAllZc();
    }

    //添加
    @Override
    public int addZc(Myzc entity) {
        return myzcMapper.addZc(entity);
    }

    //根据ID查询
    @Override
    public Myzc findZcByCid(Myzc entity) {
        return myzcMapper.findZcByCid(entity);
    }

    //修改
    @Override
    public int updateZcByCid(Myzc entity) {
        return myzcMapper.updateZcByCid(entity);
    }
}
