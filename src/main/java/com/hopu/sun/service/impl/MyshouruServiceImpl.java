package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MyshouruMapper;
import com.hopu.sun.model.Myshouru;
import com.hopu.sun.service.MyshouruService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MyshouruServiceImpl extends ServiceImpl<MyshouruMapper, Myshouru> implements MyshouruService {
    @Autowired
    private MyshouruMapper myshouruMapper;

    //查询所有
    @Override
    public List<Myshouru> findAllShouru() {
        return myshouruMapper.findAllShouru();
    }

    //添加
    @Override
    public int addShouru(Myshouru entity) {
        return myshouruMapper.addShouru(entity);
    }

    //根据ID查询
    @Override
    public Myshouru findShouruBySid(Myshouru entity) {
        return myshouruMapper.findShouruBySid(entity);
    }

    //修改
    @Override
    public int updateShouru(Myshouru entity) {
        return myshouruMapper.updateShouru(entity);
    }

}
