package com.hopu.sun.service.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.hopu.sun.mapper.MyareaMapper;
import com.hopu.sun.model.Myarea;
import com.hopu.sun.service.MyareaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MyareaServiceImpl extends ServiceImpl<MyareaMapper, Myarea> implements MyareaService {
    @Autowired
    MyareaMapper myareaMapper;

    //查询所有部门
    @Override
    public List<Myarea> findAllAreaPageData() {
        return myareaMapper.findAllAreaPageData();
    }

    //添加
    @Override
    public int addArea(Myarea entity) {
        return myareaMapper.addArea(entity);
    }

    //修改
    @Override
    public int uppArea(Myarea entity) {
        return myareaMapper.uppArea(entity);
    }

    //根据条件查询
    @Override
    public Myarea getArea(Myarea entity) {
        return myareaMapper.getArea(entity);
    }

}
