package com.hopu.sun.service;


import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Myzhichu;

import java.util.List;


public interface MyzhichuService extends IService<Myzhichu> {
    //查询所有
    List<Myzhichu> findAllzhichu();

    //添加
    int addZhichu(Myzhichu entity);

    //根据ID查询
    public Myzhichu findZhichuByZid(Myzhichu entity);

    //修改
    public int updateZhichu(Myzhichu entity);
}
