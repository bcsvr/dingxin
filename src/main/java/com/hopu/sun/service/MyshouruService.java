package com.hopu.sun.service;


import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Myshouru;

import java.util.List;


public interface MyshouruService extends IService<Myshouru> {
    //查询所有
    List<Myshouru> findAllShouru();

    //根据ID查询
    Myshouru findShouruBySid(Myshouru entity);

    //添加
    int addShouru(Myshouru entity);

    //修改
    int updateShouru(Myshouru entity);
}
