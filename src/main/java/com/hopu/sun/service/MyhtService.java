package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Myht;

import java.util.List;


public interface MyhtService extends IService<Myht> {
    //查询
    List<Myht> findAllht();

    //添加
    int addHt(Myht entity);

    //根据ID查询
    Myht findByHid(Myht entity);

    //修改
    int uppHt(Myht entity);
}
