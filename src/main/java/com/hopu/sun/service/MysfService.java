package com.hopu.sun.service;


import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Mysf;

import java.util.List;


public interface MysfService extends IService<Mysf> {
    //查询
    List<Mysf> findAllDjsf(Mysf entity);

    //添加
    int addSfXf(Mysf entity);

    //根据ID查询
    List<Mysf> findAllSfByMid(Mysf entity);
}
