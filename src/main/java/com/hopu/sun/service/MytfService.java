package com.hopu.sun.service;


import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Mytf;


public interface MytfService extends IService<Mytf> {
    //退租
    int tf(Mytf entity);
}
