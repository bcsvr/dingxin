package com.hopu.sun.service;


import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Myzc;

import java.util.List;


public interface MyzcService extends IService<Myzc> {
    //查询所有
    List<Myzc> findAllZc();

    //添加
    int addZc(Myzc entity);

    //根据ID查询
    Myzc findZcByCid(Myzc entity);

    //修改
    int updateZcByCid(Myzc entity);

}
