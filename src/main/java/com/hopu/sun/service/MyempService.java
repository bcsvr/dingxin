package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Myemp;

import java.util.List;


public interface MyempService extends IService<Myemp> {

    //查询所有员工
    List<Myemp> findAllEmp(Myemp entity);

    //登录
    Myemp login(Myemp entity);

    //根据名称查询，查看是否名称已存在
    Myemp findByEname(Myemp entity);

    //添加
    int addEmp(Myemp entity);

    //修改
    int uppEmp(Myemp entity);

    //根据ID查询
    Myemp findEmpById(Myemp entity);

    //条件查询
    Myemp findEmpByss(Myemp entity);

    //修改密码
    int uppPwd(Myemp entity);

}
