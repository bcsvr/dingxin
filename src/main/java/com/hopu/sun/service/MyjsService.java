package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myjs;

import java.util.List;


public interface MyjsService extends IService<Myjs> {
    //查询
    List<Myjs> findAllJs();

    //添加
    Msg addJs(Myjs entity);

    //根据ID查询
    Msg findJs(Myjs entity);

    //修改
    Msg updateJs(Myjs entity);
}
