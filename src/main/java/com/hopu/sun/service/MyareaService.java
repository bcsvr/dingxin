package com.hopu.sun.service;

import com.baomidou.mybatisplus.service.IService;
import com.hopu.sun.model.Myarea;

import java.util.List;


public interface MyareaService extends IService<Myarea> {

    List<Myarea> findAllAreaPageData();

    //添加
    int addArea(Myarea entity);

    //修改
    int uppArea(Myarea entity);

    //根据条件查询
    Myarea getArea(Myarea entity);


}
