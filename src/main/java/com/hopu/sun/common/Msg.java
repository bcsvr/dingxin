package com.hopu.sun.common;

import java.util.HashMap;
import java.util.Map;

public class Msg {

    private Integer code;
    private String msg;

    private Map<String, Object> map = new HashMap<String, Object>();

    public Msg add(String key, Object value) {
        this.getMap().put(key, value);
        return this;
    }


    public Msg(Integer code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }


    public Msg() {
        super();
        // TODO Auto-generated constructor stub
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

}
