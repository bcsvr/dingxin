package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mysort;

import java.util.List;


public interface MysortMapper extends BaseMapper<Mysort> {

    //查询
    List<Mysort> findMysort();

    //添加
    int addSort(Mysort entity);

    //修改
    int uppSort(Mysort entity);

    //根据条件查询
    Mysort findSortByNameOrId(Mysort entity);

}
