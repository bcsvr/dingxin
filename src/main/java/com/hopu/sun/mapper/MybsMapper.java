package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mybs;

import java.util.List;


public interface MybsMapper extends BaseMapper<Mybs> {
    //查询所有
    List<Mybs> findAllBs();
    //添加
    int addBs(Mybs entity);
    //根据ID查询
    Mybs findBsByBid(Mybs entity);
    //修改
    int updateBs(Mybs entity);

}
