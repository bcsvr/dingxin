package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mydept;

import java.util.List;


public interface MydeptMapper extends BaseMapper<Mydept> {
    //查询所有部门
    List<Mydept> findAllDept(Mydept entity);

    //添加
    int addPart(Mydept entity);

    //修改
    int updatePart(Mydept entity);

    //根据条件查询
    Mydept findPart(Mydept entity);


}
