package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Myjs;

import java.util.List;


public interface MyjsMapper extends BaseMapper<Myjs> {
    //查询所有
    List<Myjs> findAllJs();

    //添加
    int addJs(Myjs entity);

    //根据条件查询
    Myjs findJs(Myjs entity);

    //修改
    int updateJs(Myjs entity);
}
