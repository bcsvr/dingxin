package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mytf;

public interface MytfMapper extends BaseMapper<Mytf> {
    //退房
    int tf(Mytf entity);
}
