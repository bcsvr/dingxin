package com.hopu.sun.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Myemp;

import java.util.List;


public interface MyempMapper extends BaseMapper<Myemp> {
    //登录
    Myemp login(Myemp entity);

    //查询所有人员
    List<Myemp> findAllEmp(Myemp entity);

    //根据账号查询所有人员
    Myemp findByEname(Myemp entity);

    //添加
    int addEmp(Myemp entity);

    //根据ID查询
    Myemp findEmpById(Myemp entity);

    //修改
    int uppEmp(Myemp entity);

    Myemp findEmpByss(Myemp entity);

    int uppPwd(Myemp entity);
}
