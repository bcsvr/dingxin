package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Myshouru;

import java.util.List;


public interface MyshouruMapper extends BaseMapper<Myshouru> {
    //查询所有
    List<Myshouru> findAllShouru();

    //添加
    int addShouru(Myshouru entity);

    //修改
    int updateShouru(Myshouru entity);

    //根据ID查询
    Myshouru findShouruBySid(Myshouru entity);
}
