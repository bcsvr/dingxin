package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Myhouse;

import java.util.List;


public interface MyhouseMapper extends BaseMapper<Myhouse> {
    //查询所有房子
    List<Myhouse> findAllHousePD();

    //添加
    int addHouse(Myhouse entity);

    //修改
    int uppHouse(Myhouse entity);

    //根据ID查询
    Myhouse findHouseById(Myhouse entity);

    //修改状态
    int updatehouse(Myhouse entity);

    //条件查询
    List<Myhouse> findAllHouseByCondition(Myhouse entity);

    //ID查询
    List<Myhouse> findAllHouseByAidAndSid(Myhouse entity);
}
