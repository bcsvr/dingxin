package com.hopu.sun.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.hopu.sun.model.Mybiao;

import java.util.List;


public interface MybiaoMapper extends BaseMapper<Mybiao> {

    //查询
    List<Mybiao> findAllBiao();

    //添加
    int addBiao(Mybiao entity);

    //修改
    int updateMybiao(Mybiao entity);

    //根据ID查
    Mybiao findBiaoByBid(Mybiao entity);

}
