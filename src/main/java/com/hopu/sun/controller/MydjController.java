package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.common.OssUtil;
import com.hopu.sun.model.Mydj;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.model.Myhouse;
import com.hopu.sun.service.MydjService;
import com.hopu.sun.service.MyhouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;


@Controller
public class MydjController {
    @Autowired
    private MydjService mydjService;
    @Autowired
    private MyhouseService myhouseService;

    //查询所有
    @RequestMapping("/djrz/findAllDjrz.do")
    @ResponseBody
    private Msg findAllDjrz(Mydj entity,
                            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
                            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mydj> list = mydjService.findAllDjrz(entity);
        PageInfo<Mydj> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/djrz/findDjByMid.do")
    @ResponseBody
    public Msg findDjByMid(Mydj entity) {
        Mydj djByMid = mydjService.findDjByMid(entity);
        if (djByMid != null) {
            return new Msg(200, "成功！").add("djByMid", djByMid);
        } else {
            return new Msg(500, "失败！");
        }
    }

    //添加
    @RequestMapping("/djrz/addDj.do")
    @ResponseBody
    public Msg addDj(Long hid, Mydj entity, MultipartFile file, HttpServletRequest request) {
        String fileSub1 = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        Random d = new Random();
        String img1 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub1;
        // 生成文件名称
        OssUtil ossUtil = new OssUtil();

        try {
            ossUtil.uploadAliyun(file, img1);
            entity.setMimg(img1);
            entity.setMflag(0);
            //获取当前的日期
            Date date = new Date();
            //设置要获取到的时间年月日
            SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
            //获取String类型的时间
            entity.setMdate(time.format(date));
            //获取登录人ID
            Myemp myemp = (Myemp) request.getSession().getAttribute("login");
            entity.setEid(myemp.getEid());

            Myhouse myhouse = new Myhouse();
            myhouse.setHflag(1);
            myhouse.setHid(hid);
            int ii = myhouseService.updatehouse(myhouse);
            int i = mydjService.addDj(entity);
            if (i > 0 && ii > 0) {
                return new Msg(200, "成功！");
            } else {
                return new Msg(500, "失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Msg(500, "失败！");
    }

    //修改
    @RequestMapping("/djrz/updateDj.do")
    @ResponseBody
    public Msg updateDj(Mydj entity, MultipartFile file, HttpServletRequest request) {
        String fileSub1 = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        Random d = new Random();
        String img1 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub1;
        // 生成文件名称
        OssUtil ossUtil = new OssUtil();

        try {
            ossUtil.uploadAliyun(file, img1);
            entity.setMimg(img1);
            //获取登录人id
            Myemp myemp = (Myemp) request.getSession().getAttribute("login");
            entity.setEid(myemp.getEid());
            int i = mydjService.updateDj(entity);
            if (i > 0) {
                return new Msg(200, "成功！");
            } else {
                return new Msg(500, "失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Msg(500, "失败！");
    }

    //查询所有
    @RequestMapping("/djrz/findAllDjrzByCondition.do")
    @ResponseBody
    private Msg findAllDjrzByCondition(Mydj entity, Myhouse myhouse,
                                       @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
                                       @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        entity.setMyhouse(myhouse);
        List<Mydj> By = mydjService.findAllDjrzByCondition(entity);
        PageInfo<Mydj> pageInfo = new PageInfo<>(By);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //查询所有
    @RequestMapping("/djrz/findAllMydjOnDaoqi.do")
    @ResponseBody
    private Msg findAllMydjOnDaoqi(Mydj entity,
                                   @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
                                   @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mydj> list = mydjService.findAllMydjOnDaoqi(entity);
        PageInfo<Mydj> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

}


