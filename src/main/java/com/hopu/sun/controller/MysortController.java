package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Mysort;
import com.hopu.sun.service.MysortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/sun/mysort")
public class MysortController {
    @Autowired
    MysortService mysortService;

    @RequestMapping("/redirect")
    public String redirect(String page) {
        return page;
    }

    //查询
    @RequestMapping("/sort/findAllSort.do")
    @ResponseBody
    private Msg findMysort(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mysort> allSort = mysortService.findMysort();
        PageInfo<Mysort> pageInfo = new PageInfo<>(allSort);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //添加
    @RequestMapping("/sort/addSort.do")
    @ResponseBody
    public Msg addSort(Mysort entity) {
        int i = mysortService.addSort(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //修改
    @RequestMapping("/sort/uppSort.do")
    @ResponseBody
    public Msg uppSort(Mysort entity) {
        int i = mysortService.uppSort(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //根据条件查询
    @RequestMapping("/sort/findSortByNameOrId.do")
    @ResponseBody
    public Msg findSortByNameOrId(Mysort entity) {
        Mysort list = mysortService.findSortByNameOrId(entity);
        if (list != null) {
            return new Msg(200, "成功！").add("list", list);
        } else {
            return new Msg(500, "失败！");
        }
    }
}

