package com.hopu.sun.controller;

import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.common.OssUtil;
import com.hopu.sun.model.Myhouse;
import com.hopu.sun.service.MyhouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.UUID;


@Controller
public class MyhouseController {
    @Autowired
    private MyhouseService myhouseService;

    //查询
    @RequestMapping("/house/findAllHousePD.do")
    @ResponseBody
    private Msg findAllEmp(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myhouse> list = myhouseService.findAllHousePD();
        PageInfo<Myhouse> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    @RequestMapping("/redirect.do")
    public String redirect(String page) {
        return page;
    }

    //根据ID查询
    @RequestMapping("/house/findHouseById.do")
    @ResponseBody
    public Msg findHouseById(Myhouse entity) {
        Myhouse list = myhouseService.findHouseById(entity);
        String str = list.getHimg();
        String[] ss = str.split("、");
        String img1 = "";
        String img2 = "";
        String img3 = "";
        for (int q = 0; q < ss.length; q++) {
            img1 = ss[0];
            img2 = ss[1];
            img3 = ss[2];
        }
        if (list != null) {
            OssUtil ossUtil = new OssUtil();
            String imgUrl1 = ossUtil.showPic(img1);
            String imgUrl2 = ossUtil.showPic(img2);
            String imgUrl3 = ossUtil.showPic(img3);

            return new Msg(200, "成功").add("list", list).add("imgUrl1", imgUrl1).add("imgUrl2", imgUrl2).add("imgUrl3", imgUrl3);
        }
        return new Msg(500, "失败");
    }

    //修改
    @RequestMapping("/house/uppHouse.do")
    @ResponseBody
    public Msg uppHouse(Myhouse entity, MultipartFile file, MultipartFile file2, MultipartFile file3) {
        String fileSub1 = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String fileSub2 = file2.getOriginalFilename().substring(file2.getOriginalFilename().lastIndexOf("."));
        String fileSub3 = file3.getOriginalFilename().substring(file3.getOriginalFilename().lastIndexOf("."));
        Random d = new Random();
        String img1 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub1;
        String img2 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub2;
        String img3 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub3;
        // 生成文件名称
        OssUtil ossUtil = new OssUtil();
        try {
            ossUtil.uploadAliyun(file, img1);
            ossUtil.uploadAliyun(file2, img2);
            ossUtil.uploadAliyun(file3, img3);
            entity.setHimg(img1 + "、" + img2 + "、" + img3);
            int i = myhouseService.uppHouse(entity);
            if (i > 0) {
                return new Msg(200, "修改成功！");
            } else {
                return new Msg(500, "修改失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Msg(500, "修改失败！");
    }

    //添加
    @RequestMapping("/house/addHouse.do")
    @ResponseBody
    public Msg addHouse(Myhouse entity, MultipartFile file1, MultipartFile file2, MultipartFile file3, HttpServletRequest request) {
        String fileSub1 = file1.getOriginalFilename().substring(file1.getOriginalFilename().lastIndexOf("."));
        String fileSub2 = file2.getOriginalFilename().substring(file2.getOriginalFilename().lastIndexOf("."));
        String fileSub3 = file3.getOriginalFilename().substring(file3.getOriginalFilename().lastIndexOf("."));
        Random d = new Random();
        String img1 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub1;
        String img2 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub2;
        String img3 = UUID.randomUUID().toString().replace("-", "") + d.nextInt(10000) + "" + fileSub3;
        // 生成文件名称
        OssUtil ossUtil = new OssUtil();
        try {
            String uploadPath1 = ossUtil.uploadAliyun(file1, img1);
            String uploadPath2 = ossUtil.uploadAliyun(file2, img2);
            String uploadPath3 = ossUtil.uploadAliyun(file3, img3);
            entity.setHimg(img1 + "、" + img2 + "、" + img3);
            entity.setHflag(0);
            int i = myhouseService.addHouse(entity);
            if (i > 0) {
                return new Msg(200, "添加成功！");
            } else {
                return new Msg(500, "添加失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Msg(500, "添加失败！");
    }

    //查询
    @RequestMapping("/house/findAllHouseByCondition.do")
    @ResponseBody
    private Msg findAllHouseByCondition(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize, Myhouse entity
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myhouse> By = myhouseService.findAllHouseByCondition(entity);
        PageInfo<Myhouse> pageInfo = new PageInfo<>(By);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    @RequestMapping("/house/findAllHouseByAidAndSid.do")
    @ResponseBody
    public Msg findAllHouseByAidAndSid(Myhouse entity) {
        List<Myhouse> list = myhouseService.findAllHouseByAidAndSid(entity);
        if (list != null) {
            return new Msg(200, "").add("list", list);
        } else {
            return new Msg(500, "查询失败！");
        }
    }
}




