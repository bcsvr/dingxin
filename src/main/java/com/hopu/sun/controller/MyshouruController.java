package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.model.Myshouru;
import com.hopu.sun.service.MyshouruService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class MyshouruController {
    @Autowired
    private MyshouruService myshouruService;

    //查询
    @RequestMapping("/shouru/findAllShouru.do")
    @ResponseBody
    private Msg findAllShouru(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myshouru> list = myshouruService.findAllShouru();
        PageInfo<Myshouru> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //添加
    @RequestMapping("/shouru/addShouru.do")
    @ResponseBody
    public Msg addShouru(Myshouru entity, HttpServletRequest request) {
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setStime(time.format(date));
        //获取登录人ID
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        int i = myshouruService.addShouru(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/shouru/findShouruBySid.do")
    @ResponseBody
    public Msg findShouruBySid(Myshouru entity) {
        Myshouru list = myshouruService.findShouruBySid(entity);
        if (list != null) {
            return new Msg(200, "成功！").add("list", list);
        } else {
            return new Msg(500, "失败！");
        }
    }

    //修改
    @RequestMapping("/shouru/updateShouru.do")
    @ResponseBody
    public Msg updateShouru(Myshouru entity, HttpServletRequest request) {
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setStime(time.format(date));
        //获取登录人ID
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        int i = myshouruService.updateShouru(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }
}

