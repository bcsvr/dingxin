package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Mycus;
import com.hopu.sun.service.MycusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MycusController {
    @Autowired
    MycusService mycusService;

    //查询所有
    @RequestMapping("/custom/findAllCustom.do")
    @ResponseBody
    private Msg findAllCustom(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mycus> list = mycusService.findAllCustom();
        PageInfo<Mycus> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //添加
    @RequestMapping("/custom/addCus.do")
    @ResponseBody
    public Msg addCus(Mycus entity) {
        int i = mycusService.addPart(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //修改
    @RequestMapping("/custom/uppCus.do")
    @ResponseBody
    public Msg uppCus(Mycus entity) {
        int i = mycusService.uppCus(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/custom/findCusById.do")
    @ResponseBody
    public Msg findCusById(Mycus entity) {
        Mycus cusById = mycusService.findCusById(entity);
        if (cusById != null) {
            return new Msg(200, "成功！").add("cusById", cusById);
        } else {
            return new Msg(500, "失败！");
        }
    }
}



