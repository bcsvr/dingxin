package com.hopu.sun.controller;


import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Mydj;
import com.hopu.sun.model.Myemp;
import com.hopu.sun.model.Mytf;
import com.hopu.sun.service.MydjService;
import com.hopu.sun.service.MytfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MytfController {
    @Autowired
    private MytfService mytfService;

    @Autowired
    private MydjService mydjService;

    //退房
    @RequestMapping("/djsf/tf.do")
    @ResponseBody
    public Msg tf(Mytf entity, HttpServletRequest request) {

        //获取登录人id
        Myemp myemp = (Myemp) request.getSession().getAttribute("login");
        entity.setEid(myemp.getEid());
        Mydj mydj = new Mydj();
        mydj.setMflag(1);
        mydjService.updateTz(mydj);
        int i = mytfService.tf(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }
}

