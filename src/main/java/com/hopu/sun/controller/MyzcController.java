package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myzc;
import com.hopu.sun.service.MyzcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Controller
public class MyzcController {
    @Autowired
    private MyzcService myzcService;

    //查询所有
    @RequestMapping("/zc/findAllZc.do")
    @ResponseBody
    private Msg findAllZc(
            @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
            @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Myzc> list = myzcService.findAllZc();
        PageInfo<Myzc> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }

    //添加
    @RequestMapping("/zc/addZc.do")
    @ResponseBody
    public Msg addZc(Myzc entity) {
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setCtime(time.format(date));
        int i = myzcService.addZc(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }

    //根据ID查询
    @RequestMapping("/zc/findZcByCid.do")
    @ResponseBody
    public Msg findZcByCid(Myzc entity) {
        Myzc list = myzcService.findZcByCid(entity);
        if (list != null) {
            return new Msg(200, "成功！").add("list", list);
        } else {
            return new Msg(500, "失败！");
        }
    }

    //修改
    @RequestMapping("/zc/updateZcByCid.do")
    @ResponseBody
    public Msg updateZcByCid(Myzc entity) {
        //获取当前的日期
        Date date = new Date();
        //设置要获取到的时间年月日
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        //获取String类型的时间
        entity.setCtime(time.format(date));
        int i = myzcService.updateZcByCid(entity);
        if (i > 0) {
            return new Msg(200, "成功！");
        } else {
            return new Msg(500, "失败！");
        }
    }
}


