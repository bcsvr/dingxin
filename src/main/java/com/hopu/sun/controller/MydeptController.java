package com.hopu.sun.controller;


import com.baomidou.mybatisplus.plugins.pagination.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Mydept;
import com.hopu.sun.service.MydeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/sun/mydept")
public class MydeptController {
    @Autowired
    MydeptService mydeptService;

    @RequestMapping("/redirect")
    public String redirect(String page){
        return page;
    }

    //查询
    @RequestMapping("/part/findAllDept.do")
    @ResponseBody
    private Msg findMydept(Mydept entity,
                           @RequestParam(name = "current", defaultValue = "1", required = true) Integer current,
                           @RequestParam(name = "pageSize", defaultValue = "14") Integer pageSize
    ) {
        PageHelper.startPage(current, pageSize);
        List<Mydept> list = mydeptService.findAllDept(entity);
        PageInfo<Mydept> pageInfo = new PageInfo<>(list);
        if (pageInfo != null) {
            return new Msg(200, "").add("pageInfo", pageInfo);
        } else {
            return new Msg(500, "查询失败！");
        }
    }
    @RequestMapping("/part/findAllDeptName.do")
    @ResponseBody
    private Msg findMydept(Mydept entity){
        entity.setPflag(0);
        List<Mydept> list = mydeptService.findAllDept(entity);
        return new Msg(200,"").add("pageInfo",list);
    }

    //添加
    @RequestMapping("/part/addPart.do")
    @ResponseBody
    public Msg addPart(Mydept entity){
        entity.setPflag(0);
        int i = mydeptService.addPart(entity);
        if (i>0){
            return new Msg(200,"添加成功！");
        }else {
            return new Msg(500,"添加失败！");
        }
    }
    //根据条件查询
    @RequestMapping("/part/findPart.do")
    @ResponseBody
    public Msg findPart(Mydept entity){
        Mydept part = mydeptService.findPart(entity);
        if (part!=null){
            return new Msg(200,"成功").add("list",part);
        }else {
            return new Msg(500,"失败");
        }
    }
    //修改
    @RequestMapping("/part/updatePart.do")
    @ResponseBody
    public Msg updatePart(Mydept entity){
        int i = mydeptService.updatePart(entity);
        if (i>0){
            return new Msg(200,"修改成功！");
        }else {
            return new Msg(500,"修改失败！");
        }
    }
    //删除
    @RequestMapping("/part/delPart.do")
    @ResponseBody
    public Msg delPart(Integer pid) {
        Mydept mydept = new Mydept();
        mydept.setPflag(1);
        mydept.setPid(pid);
        boolean b = mydeptService.updateById(mydept);
       if (b==true){
        return new Msg(200,"删除成功！");
       }else {
            return new Msg(500,"删除失败！");
        }
    }
    //恢复
    @RequestMapping("/part/huiFuPart.do")
    @ResponseBody
    public Msg huiFuPart(Integer pid) {
        Mydept mydept = new Mydept();
        mydept.setPflag(0);
        mydept.setPid(pid);
        boolean b = mydeptService.updateById(mydept);
        if (b==true){
            return new Msg(200,"恢复成功！");
        }else {
            return new Msg(500,"恢复失败！");
        }
    }
}

