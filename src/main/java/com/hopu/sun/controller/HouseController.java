package com.hopu.sun.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HouseController {

    @RequestMapping("/redirect")
    public String redirect(String page) {
        return page;
    }

}
