package com.hopu.sun.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hopu.sun.common.Msg;
import com.hopu.sun.model.Myjs;
import com.hopu.sun.service.MyjsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MyjsController {

    @Autowired
    MyjsService myjsService;

    @RequestMapping("/myjs/findAllJs.do")
    @ResponseBody
    public Msg findAllJs(@RequestParam(value = "pn", defaultValue = "1") Integer pn) {
        PageHelper.startPage(pn, 2);

        List<Myjs> list = myjsService.findAllJs();

        PageInfo pa = new PageInfo(list, 2);
        return new Msg(200, "查询所有").add("list", pa);
    }


    @RequestMapping("/myjs/findJs.do")
    @ResponseBody
    public Msg findJs(Myjs myjs) {
        return myjsService.findJs(myjs);
    }

    @RequestMapping("/myjs/addJs.do")
    @ResponseBody
    public Msg addJs(Myjs myjs) {
        return myjsService.addJs(myjs);
    }

    @RequestMapping("/myjs/updateJs.do")
    @ResponseBody
    public Msg updateJs(Myjs myjs) {
        return myjsService.updateJs(myjs);
    }

    /*
    查询所有
     */
    @RequestMapping("/myjs/findAllJsName.do")
    @ResponseBody
    public Msg findAllJsName() {
        List<Myjs> list = myjsService.findAllJs();
        return new Msg(200, "查询所有").add("list", list);
    }
}

