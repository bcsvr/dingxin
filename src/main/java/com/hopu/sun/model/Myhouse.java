package com.hopu.sun.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Myhouse implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "hid", type = IdType.AUTO)
    private Long hid;

    private Integer sid;

    private Integer aid;

    private String haddress;

    private String hfh;

    private String hhx;

    private String hmj;

    private String hcx;

    private Float hmoney;

    private Float hwf;

    private Float hdx;

    private Float hsf;

    private Float hmq;

    private Float dkd;

    private Float skd;

    private Float mkd;

    private String hjp;

    private String hremark;

    private String himg;

    private Integer hflag;

    private Myarea myarea;

    private Mysort mysort;


}
