package com.hopu.sun.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Mysf implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "yid", type = IdType.AUTO)
    private Long yid;

    private Long mid;

    private Integer eid;

    private Float myzj;

    private String mbegintime;

    private Myhouse myhouse;

    private Myemp myemp;

    private Mycus mycus;

    private Mydj mydj;


}
