package com.hopu.sun.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Myht implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "htid", type = IdType.AUTO)
    private Integer htid;

    private String htname;

    private String htremark;


}
